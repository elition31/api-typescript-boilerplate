// Packages
import dotenv from 'dotenv';
import Server  from "./server";

// Routes
import UserRoutes from './routes/user.routes';
import AuthRoutes from './routes/auth.routes';
import TaskRoutes from './routes/task.routes';

// Services
import TasksService from './services/tasks.service';

// Utils
import { validateEnv } from './utils/env';


// Load Process Variables
dotenv.config();
validateEnv();

// Create the server
const server = new Server(
  [
    new UserRoutes(),
    new AuthRoutes(),
    new TaskRoutes()
  ]
  );

// Start the server
server.start();

export const tasks = new TasksService();