// Packages
import nodemailer from "nodemailer";

// Models
import { EmailTemplate } from "../models/interfaces/email-template";

interface Email {
  from: string;
  to: string;
  subject: string;
  html: string;
}

export default class MailService {
  private transporter: nodemailer.Transporter;
  private message: Email = { from: "", to: "", subject: "", html: "" };

  constructor() {
    const config: any = {
      host: process.env.EMAIL_HOSTNAME || '',
      port: process.env.EMAIL_PORT || 465,
      secure: process.env.EMAIL_SECURE || true,
      user: process.env.EMAIL_ADDRESS || '',
      pass: process.env.EMAIL_PASSWORD || '',
    }
    this.transporter = nodemailer.createTransport({
      host: config.host,
      port: config.port,
      secure: config.secure,
      auth: {
        user: config.user,
        pass: config.pass
      }
    });
  }

  /**
   * Set Email options like the recipient, subject and email template
   */
  public setEmail(recipient: string, subject: string, data: EmailTemplate): void {
    this.message = {
      from: `Service <${process.env.EMAIL_ADDRESS}>`,
      to: recipient,
      subject: subject,
      html: this.createHtml(data)
    };
  }

  /**
   * Send the email
   */
  public send(): any {
    return new Promise((resolve, reject) => {
      this.transporter.sendMail(this.message, (error, info) => {
        if (error) {
          reject(error);
        }
        resolve(info);
      });
    });
  }

  /**
   * Return an HTML template as String
   */
  private createHtml(data: EmailTemplate): string {
    return `<h1>${data.title}</h1> <br> <p>${data.text}</p> <br> ${data.link}`;
  }
}
