// Packages
import * as fs from "fs";
import Log from "../utils/log";

export default class LoggerService {

  /**
   * Method that log informations into file
   * @param {string} type the type of the log (ex: server)
   * @param {string} data message to add in the log
   */
  public static async addLog(type: string, title: string, data: any): Promise<undefined> {
    let log = new Log(type, title, data);
    
    let isFolderExist: boolean = await LoggerService.isFolderOrFileExist(log.getFolderPath());

    if (!isFolderExist) {
      isFolderExist = await LoggerService.createFolder(log.getFolderPath());
      if (!isFolderExist) {
        console.log(`An error occured when server tried to create ${log.getFullPath()} log file`);
        return undefined;
      }
    }
    
    let isFileExist: boolean = await LoggerService.isFolderOrFileExist(log.getFullPath());

    if (!isFileExist) {
      isFileExist = await LoggerService.createFile(log.getFullPath());
    }
    
    try {
      const path: string = log.getFullPath();
      const logValue: string = log.getLog();
      fs.appendFileSync(path, logValue);
    } catch (error) {
      console.log(error);
    }
  }

  /**
   * Checks if a folder or file exist
   * @param {string} item can be he path of a folder or a file
   */
  private static isFolderOrFileExist(item: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      resolve(fs.existsSync(item));
    });
  }

  /**
   * Create a folder with recursive option
   * @param {string} path the path of the last folder
   */
  private static createFolder(path: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        fs.mkdirSync(path, { recursive: true });
        resolve(true);
      } catch (error) {
        console.log(error);
        reject(false)
      }
    });
  }

  /**
   * Create a file
   * @param {string} filePath the path include the file itself
   */
  private static createFile(filePath: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      try {
        fs.writeFileSync(filePath, "");
        resolve(true);
      } catch (error) {
        console.log(error);
        reject(false)
      }
    });
  }
}