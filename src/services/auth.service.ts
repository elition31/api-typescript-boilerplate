// Packages
import bcrypt from "bcrypt";

// Services
import MailService from "./mail.service";
import LoggerService from "./logger.service";

// Utils
import TokenUtility from "../utils/token-utility";
import CronTask from "../utils/cron-task";
import { deleteToken } from "../utils/tasks-list";

// Models and Schemas
import { EmailTemplate } from "../models/interfaces/email-template";
import TokenSchema from '../models/schemas/token.schema';

// Data
import { NEW_PASSWORD_FR, NEW_PASSWORD_EN } from "../models/emails/new-password";
import { tasks } from "..";

const Token = TokenSchema;

export default class AuthService {
  /**
   * Compare encrypted password with a password given by a user
   */
  public static async comparePassword(password: string, hashedPassword: string): Promise<boolean> {
    return new Promise((resolve, reject) => {
      bcrypt.compare(password, hashedPassword, (error, match) => {
        if (match) {
          resolve(true);
        } else {
          resolve(false); 
        }
      });
    })
  }

  /**
   * Send an email with a link that redirect to a page where user can create new password.
   * The token is valid for 1 hour.
   * @param {string} email Email Address of the recipient
   * @param {string} language language of the mail (FR|EN)
   */
  public static async sendNewPassword(email: string, language: string): Promise<void> {
    const mailService: MailService = new MailService();

    // Get email template
    const template: EmailTemplate = language === 'FR' ? NEW_PASSWORD_FR : NEW_PASSWORD_EN;

    // Generate token
    const token: string = TokenUtility.sign({email: email}, "1h");

    
    let newToken = new Token({ value: token })

    // Store the token in database
    newToken.save((error, result) => {
      if (error) {
        console.log(error);
      } else {
        // Create new CRON Job
        const newJob = new CronTask(result.createdDate);

        // Start the cronJob
        const task = newJob.start(() => deleteToken(result.value, newToken._id), 1, "hours");

        // Add the task to the list
        if (task !== null) {
          tasks.addTask({id: newToken._id, cronJob: task, description: `Suppression du token gérant la demande de changement du mot de passe de ${email}`});
        }
      }
    });

    // Create link
    template.link = `${process.env.EMAIL_PAGE_LINK}/${token}`;

    // Set email
    mailService.setEmail(email, 'Demande de mot de passe', template);

    try {
      // Send Email
      let info = await mailService.send();
      LoggerService.addLog('service', `New password request has been sent to ${email}`, null);
    } catch (error) {
      let data: string = '';

      if (error.response !== undefined) {
        data = error.response.split('\n').join(' ');
      } else {
        data = 'Undefined error occured when mailService tried to send an email';
      }

      LoggerService.addLog('services', data, error);
    }
  }
}
