// Models
import { Task } from "../models/interfaces/task.interface";

export default class TasksService {
  private tasks: Task[] = [];

  constructor() { }

  /**
   * Returns all the current tasks
   */
  public getTasks(): Task[] {
    return this.tasks;
  }

  /**
   * Returns a task by its ID
   * @param id 
   */
  public getTask(id: string): Task | undefined {
    return this.tasks.find(task => task.id == id);
  }

  /**
   * Add a new task to the list
   * @param task 
   */
  public addTask(task: Task): void {
    this.tasks.push(task);
  }

  /**
   * Remove the task of the list
   * @param id 
   */
  public removeTask(id: string): void {
    const task: Task | undefined = this.getTask(id);

    if (task != undefined) {
      task.cronJob.stop();
    }
  
    this.tasks = this.tasks.filter((value, index, arr) => {
      return value.id.toString() != id;
    });
  }
}