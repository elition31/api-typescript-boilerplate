// Messsages
import { EN_ERROR_MESSAGE, EN_SUCCESS_MESSAGE } from "../messages/en";
import { FR_ERROR_MESSAGE, FR_SUCCESS_MESSAGE } from "../messages/fr";

export default class MessageService {

  /**
   * Return an error message in the chosen language
   */
  public static getErrorMessage(id: number, language: string, category: string): Message {
    let errorMessage: Message | undefined;

    if (language === "FR" && MessageService.isCategoryExist(category)) {
      errorMessage = FR_ERROR_MESSAGE[category].find(message => message.id === id);
    } else if (MessageService.isCategoryExist(category)) {
      errorMessage = EN_ERROR_MESSAGE[category].find(message => message.id === id);
    }

    return errorMessage === undefined ? MessageService.getDefaultErrorMessage(language): errorMessage;
  }

  /**
   * Return a success message in the chosen language
   */
  public static getSuccessMessage(id: number, language: string, category: string): Message {
    let successMessage: Message | undefined;

    if (language === "FR" && MessageService.isCategoryExist(category)) {
      successMessage = FR_SUCCESS_MESSAGE[category].find(message => message.id === id);
    } else if (MessageService.isCategoryExist(category)) {
      successMessage = EN_SUCCESS_MESSAGE[category].find(message => message.id === id);
    }

    return successMessage === undefined ? MessageService.getDefaultSuccessMessage(language): successMessage;
  }

  /**
   * Return the default error message
   */
  private static getDefaultErrorMessage(language: string): Message {
    return { id: 0, message: language === 'FR' ? "Une erreur a eu lieu.": "An error has occurred.", code: 500 };
  }

  /**
   * Return the default success message
   */
  private static getDefaultSuccessMessage (language: string): Message {
    return { id: 0, message: language === 'FR' ? "Opération Valide.": "Valid Operation.", code: 200 };
  }

  /**
   * Check if the catogory given exist
   */
  private static isCategoryExist(category: string): boolean {
    const list: string[] = ["user", "service", "server", "database"];
    return list.includes(category);
  }
}