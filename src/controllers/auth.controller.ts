// Packages
import { Request, Response, NextFunction } from "express";
import bcrypt from "bcrypt";

// Services
import MessageService from "../services/message.service";
import AuthService from "../services/auth.service";
import TokenUtility from "../utils/token-utility";
import LoggerService from "../services/logger.service";

// Schemas and Models
import UserSchema from "../models/schemas/user.schema";
import TokenSchema from "../models/schemas/token.schema";

import { tasks } from "..";

const User = UserSchema;
const Token = TokenSchema;
const domainNotAllowed: string[] = ["yopmail.com", "yopmail.fr", "yopmail.net", "jetable.fr.nf", "nospam.ze.tc", "nomail.xl.cx", "mega.zik.dj", "speed.1s.fr", "cool.fr.nf", "courriel.fr.nf", "moncourrier.fr.nf", "monemail.fr.nf", "monmail.fr.nf"];

export class AuthController {
  /**
   * Login method
   */
  public login(request: Request, response: Response, next: NextFunction) {
    console.info("login");

    const { username, password } = request.body;
    let message: Message;
  
    User.findOne({username : username}, async (error, user) => {
      if (error) {
        message = MessageService.getErrorMessage(0, request.params.lang, "user");

        response.status(message.code).json({ message: message.message, data: error });
      } else {
        if (user) {
          let isValid = await AuthService.comparePassword(password, user.password);

          if (isValid) {
            // Set the payload
            const payload = {
              _id: user._id,
              username: user.username,
              firstName: user.firstName,
              lastName: user.lastName,
              email: user.email,
              role: user.role,
              createdAt: user.createdDate,
              updatedAt: user.updatedDate,
            }

            const token = TokenUtility.sign(payload, "1h");
            
            message = MessageService.getSuccessMessage(6, request.params.lang, "user");
            response.status(message.code).json({ message: message.message, data: { user: user, token: token } });
          } else {
            message = MessageService.getErrorMessage(6, request.params.lang, "user");
            response.status(message.code).json({ message: message.message, data: null });
          }
        } else {
          message = MessageService.getErrorMessage(6, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        }
      }
    });
  }

  /**
   * Allow a user to get a new password
   */
  public rememberPassword(request: Request, response: Response, next: NextFunction) {
    console.info("rememberPassword");

    const { email } = request.body;
    let message: Message;
  
    User.findOne({email : email}, (error, user) => {
      if (error) {
        message = MessageService.getErrorMessage(0, request.params.lang, "user");

        response.status(message.code).json({ message: message.message, data: error });
      } else {
        if (user) {
          // Send new password
          AuthService.sendNewPassword(user.email, request.params.lang);

          message = MessageService.getSuccessMessage(7, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        } else {
          message = MessageService.getErrorMessage(7, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        }
      }
    });
  }

  public updateUserPassword(request: Request, response: Response, next: NextFunction) {
    console.info("updateUserPassword");

    let message: Message;
    
    const { token, password } = request.body;
    
    const payload: any = TokenUtility.verify(token, '1h');

    const email = payload.email;

    if (email === undefined || email === '') {
      message = MessageService.getErrorMessage(10, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (password === undefined || password === '') {
      message = MessageService.getErrorMessage(11, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else {
      User.findOne({email : email}, (error, user) => {
        if (error) {
          message = MessageService.getErrorMessage(0, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: error });
        } else {
          if (user) {
            bcrypt.hash(password, 10, async function(err, hash) {
              if (err) {
                LoggerService.addLog('user', `User [${user.email}] had an error during the hash`, err);

                message = MessageService.getErrorMessage(9, request.params.lang, "user");
                response.status(message.code).json({ message: message.message, data: null });
              } else {
                user.password = hash;

                const updateResult = await User.updateOne({ _id: user._id }, user);
                
                const res = await Token.findOne({ value: token.split(' ')[1] }, async (error, result) => {
                  if (error) {
                    console.log("error");
                  } else {
                    const deleteResult = await Token.deleteOne({ value: token.split(' ')[1] }, (error) => {
                      if (result !== null) {
                        const task = tasks.getTask(result._id.toString());
                        if (task) {
                          task.cronJob.stop();
                          tasks.removeTask(result._id.toString());
                        }
                      }
                    });
                  }
                });

                message = MessageService.getSuccessMessage(9, request.params.lang, "user");
                response.status(message.code).json({ message: message.message, data: null });
              }
            });
          } else {
            message = MessageService.getErrorMessage(9, request.params.lang, "user");
            response.status(message.code).json({ message: message.message, data: null });
          }
        }
      });
    }
  }

  /**
   * Register method
   */
  public register(request: Request, response: Response, next: NextFunction) {
    console.info("register");

    let message: Message;

    const data: any = request.body;
    
    if (data.username === undefined || data.username === "") {
      message = MessageService.getErrorMessage(12, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (data.email === undefined || data.email === "") {
      message = MessageService.getErrorMessage(10, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (domainNotAllowed.includes(data.email.split("@")[1])) {
      message = MessageService.getErrorMessage(13, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (data.password === undefined || data.password === "") {
      message = MessageService.getErrorMessage(11, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (data.firstName === undefined || data.firstName === "") {
      message = MessageService.getErrorMessage(14, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else if (data.lastName === undefined || data.lastName === "") {
      message = MessageService.getErrorMessage(15, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    } else {
      let newUser = new User(data);
  
      newUser.save((error, user) => {
        if (error) {
          message = MessageService.getErrorMessage(2, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        } else {
          message = MessageService.getSuccessMessage(8, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        }
      });
    }
  }
}