// Packages
import { Request, Response, NextFunction } from "express";

// Services
import MessageService from "../services/message.service";

// Utils
import { tasks } from "..";

// Models and Schemas
import { Task } from "../models/interfaces/task.interface";

export class TaskController {
  /**
   * Returns all current tasks
   * @param request 
   * @param response 
   * @param next 
   */
  public fetchTasks(request: Request, response: Response, next: NextFunction) {
    let message: Message;
    
    message = MessageService.getSuccessMessage(1, request.params.lang, "service");
    const tasksList: any[] = [];
    
    tasks.getTasks().forEach(t => {
      if (t != null) {
        tasksList.push({id: t.id, description: t.description, date: t.cronJob.nextDate().toDate()});
      }
    })
  
    return response.status(message.code).json({ message: message.message, data: tasksList });
  }

  /**
   * Returns task's informations
   * @param request 
   * @param response 
   * @param next 
   */
  public fetchTaskById(request: Request, response: Response, next: NextFunction) {
    let message: Message;
    
    const task = tasks.getTask(request.params.id);
    let data = null;
    
    if (task !== undefined) {
      data = {id: task.id, description: task.description, date: task.cronJob.nextDate().toDate()}
      message = MessageService.getSuccessMessage(2, request.params.lang, "service");
    } else {
      message = MessageService.getErrorMessage(3, request.params.lang, "service");
    }

    return response.status(message.code).json({ message: message.message, data: data });
  }

  /**
   * Delete task by its ID
   * @param request 
   * @param response 
   * @param next 
   */
  public async deleteTask(request: Request, response: Response, next: NextFunction) {
    let message: Message;

    const task: Task | undefined = tasks.getTask(request.params.id);

    if (task === undefined) {
      message = MessageService.getErrorMessage(3, request.params.lang, "service");
    } else {
      tasks.removeTask(request.params.id);
      message = MessageService.getSuccessMessage(3, request.params.lang, "service");
    }

    return response.status(message.code).json({ message: message.message, data: null });
  };
}