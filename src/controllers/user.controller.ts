// Packages
import { Request, Response, NextFunction } from "express";

// Services
import MessageService from "../services/message.service";

// Schemas and Models
import UserSchema from "../models/schemas/user.schema";

const User = UserSchema;

export class UserController {
  /**
   * Return all the users
   */
  public fetchUsers(request: Request, response: Response, next: NextFunction) {
    console.info("fetchUsers");
    let message: Message;
  
    User.find({}, (error, users) => {
      if (error) {
        message = MessageService.getErrorMessage(0, request.params.lang, "user");

        response
          .status(message.code)
          .json({ message: message.message, data: error });
      } else {
        if (users.length === 0) {
          message = MessageService.getErrorMessage(5, request.params.lang, "user");

          response
            .status(message.code)
            .json({ message: message.message, data: null });
        } else {
          message = MessageService.getSuccessMessage(5, request.params.lang, "user");

          response
            .status(message.code)
            .json({ message: message.message, data: users });
        }
      }
    });
  };

  /**
   * Return one user by its ID
   */
  public fetchUserById(request: Request, response: Response, next: NextFunction) {
    console.info("fetchUserById");

    let message: Message;

    User.findById(request.params.id, (error, user) => {
      if (error) {
        message = MessageService.getErrorMessage(1, request.params.lang, "user");

        response
          .status(message.code)
          .json({ message: message.message, data: null });
      } else {
        message = MessageService.getSuccessMessage(4, request.params.lang, "user");

        response
          .status(message.code)
          .json({ message: message.message, data: user });
      }
    });
  };

  /**
   * Return one user by its ID
   */
  public fetchUserByEmail(request: Request, response: Response, next: NextFunction) {
    console.info("fetchUserByEmail");

    let message: Message;

    User.find({ email: request.params.email }, (error, user) => {
      if (error) {
        message = MessageService.getErrorMessage(0, request.params.lang, "user");

        response
          .status(message.code)
          .json({ message: message.message, data: error });
      } else {
        if (user.length === 0) {
          message = MessageService.getErrorMessage(1, request.params.lang, "user");

          response
            .status(message.code)
            .json({ message: message.message, data: null });
        } else {
          message = MessageService.getSuccessMessage(4, request.params.lang, "user");

          response
            .status(message.code)
            .json({ message: message.message, data: user });
        }
      }
    });
  };

  /**
   * Add a new user
   */
  public addUser(request: Request, response: Response, next: NextFunction) {
    console.info("addUser");

    let message: Message;

    let newUser = new User(request.body);

    newUser.save((error, user) => {
      if (error) {
        message = MessageService.getErrorMessage(2, request.params.lang, "user");

        response
          .status(message.code)
          .json({ message: message.message, data: null });
      } else {
        message = MessageService.getSuccessMessage(1, request.params.lang, "user");

        response
          .location(`/users/one/id/${user.id}`)
          .status(message.code)
          .json({ message: message.message, data: user });
      }
    });
  };

  /**
   * Update a user
   */
  public async updateUser(request: Request, response: Response, next: NextFunction) {
    console.info("updateUser");
    
    let message: Message;

    const res = await User.updateOne({ _id: request.params.id }, request.body.data );

    if (res.n === 1) {
      message = MessageService.getSuccessMessage(2, request.params.lang, "user");

      response
        .status(message.code)
        .json({ message: message.message, data: null });
    } else {
      message = MessageService.getErrorMessage(3, request.params.lang, "user");

      response
        .location(`/users/one/id/${request.params.id}`)
        .status(message.code)
        .json({ message: message.message, data: null });
    }
  };

  /**
   * Delete a user
   */
  public async deleteUser(request: Request, response: Response, next: NextFunction) {
    console.info("deleteUser");

    let message: Message;

    const res = await User.deleteOne({ _id: request.params.id });

    if (res.n === 1) {
      message = MessageService.getSuccessMessage(3, request.params.lang, "user");

      response
        .status(message.code)
        .json({ message: message.message, data: null });
    } else {
      message = MessageService.getErrorMessage(4, request.params.lang, "user");

      response
        .status(message.code)
        .json({ message: message.message, data: null });
    }
  };
}
