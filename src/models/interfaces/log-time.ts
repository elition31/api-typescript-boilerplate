export interface LogTime {
  year: string;
  month: string;
  day: string;
  hour: string;
  minute: string;
  second: string;
}