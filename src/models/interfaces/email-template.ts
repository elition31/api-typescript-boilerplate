export interface EmailTemplate {
  title: string;
  text: string;
  link?: string;
}