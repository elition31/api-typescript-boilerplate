interface Message {
  id: number;
  message: string;
  code: number;
}
