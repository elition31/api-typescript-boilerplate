import { CronJob } from "cron";

export interface Task {
  id: string;
  description: string;
  cronJob: CronJob;
}