export interface LogFolder {
  year: string;
  month: string;
  day: string;
}