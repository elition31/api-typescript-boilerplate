interface ErrorMessage {
  [user: string]: Message[];
  server: Message[];
  database: Message[];
  service: Message[];
}