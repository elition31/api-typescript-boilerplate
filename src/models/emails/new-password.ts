// Models
import { EmailTemplate } from "../interfaces/email-template"

export const NEW_PASSWORD_FR: EmailTemplate = {
  title: "Nouveau mot de passe",
  text: "Vous avez fait la demande d'un nouveau mot de passe. <br> Nous vous invitons à cliquer sur le lien pour créer un nouveau mot de passe. <br><br> Si vous n'avez pas fait cette demande, merci de ne pas tenir compte de cet email."
}

export const NEW_PASSWORD_EN: EmailTemplate = {
  title: "New password",
  text: "You have requested a new password. <br> We invite you to click on the link to create a new password. <br> <br> If you have not made this request, please ignore this email."
}