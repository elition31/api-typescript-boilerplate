// Packages
import mongoose, { Schema, Document } from "mongoose";

export interface IToken extends Document {
  _id: string;
  value: string;
  createdDate: Date;
}

const TokenSchema: Schema = new Schema({
  value: { type: String, required: true, unique: true },
  createdDate: {
    type: Date,
    default: new Date()
  },
});

// Export the model and return your IUser interface
export default mongoose.model<IToken>("Token", TokenSchema);
