// Packages
import mongoose, { Schema, Document } from "mongoose";
import bcrypt from "bcrypt";

export interface IUser extends Document {
  _id: string;
  username: string;
  email: string;
  firstName: string;
  lastName: string;
  password: string;
  role: "ADMIN" | "USER";
  createdDate: Date;
  updatedDate: Date;
}

const ROLES: string[] = ["ADMIN", "USER"];

const UserSchema: Schema = new Schema({
  username: { type: String, required: true, unique: true },
  email: { type: String, required: true, unique: true },
  firstName: { type: String, required: true },
  lastName: { type: String, required: true },
  password: { type: String, required: true },
  role: { type: String, enum: ROLES, default: "USER", required: true },
  createdDate: {
    type: Date,
    default: new Date().setHours(new Date().getHours() + 1)
  },
  updatedDate: {
    type: Date,
    default: new Date().setHours(new Date().getHours() + 1)
  }
});

UserSchema.pre<IUser>('save', function(next) {
  const user = this;
  if(!user.isModified || !user.isNew) { // don't rehash if it's an old user
    next();
  } else {
    bcrypt.hash(user.password, 10, function(err, hash) {
      if (err) {
        console.log('Error hashing password for user', user.username);
        next(err);
      } else {
        user.password = hash;
        next();
      }
    });
  }
});

// Export the model and return your IUser interface
export default mongoose.model<IUser>("User", UserSchema);
