// Packages
import { CronJob } from "cron";

import moment from 'moment';

export default class CronTask {
  public date: Date;
  
  constructor(date: Date) {
    this.date = date;
  }

  public start(task: () => any, value: number, unit: "seconds" | "minutes" | "hours" | "days"): CronJob | null {
    try {
      // Create the CronJob
      const job = new CronJob(moment(this.date).add(value, unit).toDate(), task);

      // Start
      job.start();
      
      return job;
    } catch (error) {
      console.error(error);
      return null;
    }
  }
}