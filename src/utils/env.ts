import { cleanEnv, str, port, bool } from 'envalid';

export function validateEnv(): void {
  cleanEnv(process.env, {
    BUILD: str(),
    API_PORT: port(),
    API_HOSTNAME: str(),
    JWT_SECRET: str(),
    DB_PORT: port(),
    DB_HOSTNAME: str(),
    DB_DATABASE: str(),
    DB_USER: str(),
    DB_PASSWORD: str(),
    EMAIL_HOSTNAME: str(),
    EMAIL_PORT: port(),
    EMAIL_SECURE: bool(),
    EMAIL_ADDRESS: str(),
    EMAIL_PASSWORD: str(),
    EMAIL_PAGE_LINK: str(),
  });
}