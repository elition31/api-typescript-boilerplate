// Services
import LoggerService from "../services/logger.service";

// Schemas and Models
import TokenSchema from "../models/schemas/token.schema";
import { tasks } from "..";

const Token = TokenSchema;

export function deleteToken(token: string, taskId: string): any {
  Token.deleteOne({ value: token }).then((result) => {
    LoggerService.addLog("service", `the token [${token}] has been deleted`, null);
  });

  tasks.removeTask(taskId);
}