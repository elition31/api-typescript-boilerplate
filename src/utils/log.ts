import { LogFolder } from "../models/interfaces/log-folder";
import { LogTime } from "../models/interfaces/log-time";

export default class Log {
  private folder: LogFolder;
  private fileName: string;
  private title: string = "";
  private data: any;
  private time: LogTime;

  constructor(fileName: string, title: string, data: any) {
    this.time = this.setTime();
    this.title = title;
    this.data = data;
    
    this.folder = {
      year: this.time.year,
      month: this.time.month,
      day: this.time.day
    }

    this.fileName = `${fileName}.txt`;
  }

  public getFolderPath(): string {
    return `logs/${this.folder.year}/${this.folder.month}/${this.folder.day}`;
  }

  public getFullPath(): string {
    return `logs/${this.folder.year}/${this.folder.month}/${this.folder.day}/${this.fileName}`;
  }

  public getLog(): string {
    const trace: string = this.data === null ? "" : `\n ${this.data}`;
    return `${this.time.day}/${this.time.month}/${this.time.year} - ${this.time.hour}:${this.time.minute}:${this.time.second} | ${this.title} ${trace}\n`;
  }

  private setTime(): LogTime {
    const date = new Date();

    return {
      year: date.getFullYear().toString(),
      month: date.getMonth() + 1 > 9 ? `${date.getMonth() + 1}` : `0${date.getMonth() + 1}`,
      day: date.getDate() > 9 ? `${date.getDate()}` : `0${date.getDate()}`,
      hour: date.getUTCHours() > 9 ? `${date.getUTCHours()}` : `0${date.getUTCHours()}`,
      minute: date.getUTCMinutes() > 9 ? `${date.getUTCMinutes()}` : `0${date.getUTCMinutes()}`,
      second: date.getUTCSeconds() > 9 ? `${date.getUTCSeconds()}` : `0${date.getUTCSeconds()}`
    }
  }
}