// Packages
import * as jwt from "jsonwebtoken";

export default class TokenUtility {
  /**
   * Return a signed token
   * @param {any} payload the payload
   * @param {string} expiration duration of token (e.g. 1d or 1h)
   */
  public static sign(payload: any, expiration: string): string {
    // Set Token Expiration
    const options: jwt.SignOptions = {
      expiresIn: expiration
    };

    // Set Secret
    const secret: string = process.env.JWT_SECRET || '';
    
    return jwt.sign(payload, secret, options);
  }

  /**
   * 
   */
  public static verify(token: string, expiration: string): string | object {
    // Set Secret
    const secret: string = process.env.JWT_SECRET || '';

    // Parse Token
    token = token.split(' ')[1];

    // Set Token Expiration
    const options: jwt.SignOptions = {
      expiresIn: expiration
    };

    return jwt.verify(token, secret, options);
  }
}