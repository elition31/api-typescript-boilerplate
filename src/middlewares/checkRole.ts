// Packges
import { Request, Response, NextFunction } from "express";

// Services
import MessageService from "../services/message.service";
import TokenUtility from "../utils/token-utility";

/**
 * Check if the user can access to the based on its role
 * @param request 
 * @param response 
 * @param next 
 */
export const checkRole = async (request: Request, response: Response, next: NextFunction) => {
  // Init response message
  let message: Message;

  // Check routes authorization
  const authAsAdmin = await mustBeAdmin(request.url);
  const authAsUser = await mustBeUser(request.url);

  if (authAsAdmin || authAsUser) {
    if (request.headers.authorization !== undefined) {
      const payload: any = TokenUtility.verify(request.headers.authorization, "1h");

      if (authAsAdmin) {
        if (payload.role == "ADMIN") {
          next();
        } else {
          message = MessageService.getErrorMessage(8, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        }
      } else {
        if (payload.role == "USER") {
          next();
        } else {
          message = MessageService.getErrorMessage(8, request.params.lang, "user");
          response.status(message.code).json({ message: message.message, data: null });
        }
      }
    } else {
      message = MessageService.getErrorMessage(8, request.params.lang, "user");
      response.status(message.code).json({ message: message.message, data: null });
    }
  } else {
    next();
  }
}

/**
 * Returns True if the route requires to be an administrator
 * @param url 
 */
const mustBeAdmin = (url: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    if (url.includes("auth")) {
      resolve(false)
    } else if (url.includes("users/update")) {
      resolve(false);
    } else {
      resolve(true);
    }
  });
}

/**
 * Returns True if the route requires to be a user
 * @param url 
 */
const mustBeUser = (url: string): Promise<boolean> => {
  return new Promise((resolve, reject) => {
    if (url.includes("users/update")) {
      resolve(true);
    } else {
      resolve(false);
    }
  });
}