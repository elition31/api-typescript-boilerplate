// Packges
import { Request, Response, NextFunction } from "express";

// Services
import MessageService from "../services/message.service";
import TokenUtility from "../utils/token-utility";

// Schemas and Models
import TokenSchema from '../models/schemas/token.schema';

const Token = TokenSchema;

export const checkJwt = (request: Request, response: Response, next: NextFunction) => {
  // Get the jwt token from the headers
  let token: string | undefined = request.headers['authorization'];
  let message: Message;

  if (token !== undefined) {
    if (process.env.JWT_SECRET !== undefined) {
      try {
        let jwtPayload = TokenUtility.verify(token, '1h');
        next();
      } catch (error) {
        message = MessageService.getErrorMessage(8, request.params.lang, "user");
        response.status(message.code).json({ message: message.message, data: null });
      }
    } else {
      message = MessageService.getErrorMessage(1, request.params.lang, "server");
      response.status(message.code).json({ message: message.message, data: null });
    }
  } else {
    message = MessageService.getErrorMessage(8, request.params.lang, "user");
    response.status(message.code).json({ message: message.message, data: null });
  }
}

export const checkTokenPassword = (request: Request, response: Response, next: NextFunction) => {
  let token: string = request.body.token;
  let message: Message;

  if (token !== undefined) {
    if (process.env.JWT_SECRET !== undefined) {
      try {
        let jwtPayload = TokenUtility.verify(token, '1h');
        
        Token.findOne({value: token.split(' ')[1]}, (error, result) => {
          if (error) {
            message = MessageService.getErrorMessage(1, request.params.lang, "service");
            response.status(message.code).json({ message: message.message, data: null });
          } else {
            next();
          }
        });
      } catch (error) {
        message = MessageService.getErrorMessage(1, request.params.lang, "service");
        response.status(message.code).json({ message: message.message, data: null });
      }
    } else {
      message = MessageService.getErrorMessage(1, request.params.lang, "server");
      response.status(message.code).json({ message: message.message, data: null });
    }
  } else {
    message = MessageService.getErrorMessage(2, request.params.lang, "service");
    response.status(message.code).json({ message: message.message, data: null });
  }
}