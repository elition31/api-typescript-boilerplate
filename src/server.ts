// Packages
import express from "express";
import * as bodyParser from "body-parser";
import cookieParser from "cookie-parser";
import cors from "cors";
import mongoose from "mongoose";

// Services
import LoggerService from "./services/logger.service";

// Schemas and Models
import Route from "./models/interfaces/route";

export default class Server {
  readonly port: number | string;
  public app: express.Application;
  public isReady: boolean = false;

  /**
   * Constructor
   * @param routes An array of routes
   */
  constructor(routes: Array<Route>) {
    this.app = express();
    this.port = process.env.API_PORT || 3000;
    this.initialization(routes);
  }

  /**
   * Initialize Database, Middleware and routes
   */
  private async initialization(routes: Array<Route>): Promise<void> {
    this.isReady = await this.connectionToDataBase(process.env.BUILD);
    this.initializeMiddleware();
    this.initializeRoutes(routes);
  }

  /**
   * Start the server
   */
  public start(): void {
    let id = setInterval(() => {
      if (this.isReady) {
        clearInterval(id);

        this.app.listen(this.port, () => {
          LoggerService.addLog("server", `The Server is ready and listening on ${this.port}`, null );
        });
      } else {
        LoggerService.addLog("server", "The server is not ready yet !", null);
      }
    }, 1000);
  }

  /**
   * Return the server instance
   */
  public getServer(): express.Application {
    return this.app;
  }

  /**
   * Initialize all the middlewares
   */
  private initializeMiddleware(): void {
    this.app.use(bodyParser.json());
    this.app.use(cookieParser());
    this.app.use(cors());
  }

  /**
   *  Initialize all the routes
   */
  private initializeRoutes (routes: Array<Route>): void {
    routes.forEach(routes => {
      this.app.use("/api/v1", routes.router);
    });
  };

  /**
   * Initialize the connection with the database
   */
  private connectionToDataBase(env: string | undefined): Promise<boolean> {
    return new Promise((resolve, reject) => {
      let url: string = "";

      // Set MongoDB URL based on the environment
      if (env === "PROD") {
        url = `mongodb://${process.env.DB_USER}:${process.env.DB_PASSWORD}@${process.env.DB_HOSTNAME}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;
      } else {
        url = `mongodb://${process.env.DB_HOSTNAME}:${process.env.DB_PORT}/${process.env.DB_DATABASE}`;
      }

      // MongoDB mandatory options
      mongoose.set("useCreateIndex", true);

      // Try to connect to the database for the first time
      mongoose
        .connect(url, { useNewUrlParser: true, useUnifiedTopology: true })
        .then(() => {
          LoggerService.addLog("server", "The server is connected to the database", null);
          resolve(true);
        })
        .catch(error => {
          LoggerService.addLog("server", "ERROR - The server can't connected to the database", error);
          resolve(false);
        });
    });
  };
}
