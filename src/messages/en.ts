export const EN_ERROR_MESSAGE: ErrorMessage = {
  user: [
    {
      id: 1,
      message: "User does not exist.",
      code: 404
    },
    {
      id: 2,
      message: "A user already exist with this email.",
      code: 409
    },
    {
      id: 3,
      message: "The user has not been updated.",
      code: 409
    },
    {
      id: 4,
      message: "The user has not been deleted.",
      code: 409
    },
    {
      id: 5,
      message: "No users were found.",
      code: 404
    },
    {
      id: 6,
      message: "Username or password is not correct.",
      code: 401
    },
    {
      id: 7,
      message: "This email address does not match any users.",
      code: 404
    },
    {
      id: 8,
      message: "You do not have the necessary authorization to perform this action.",
      code: 403
    },
    {
      id: 9,
      message: "The password has not been updated.",
      code: 409
    },
    {
      id: 10,
      message: "The email address is missing.",
      code: 404
    },
    {
      id: 11,
      message: "The password is missing.",
      code: 404
    },
    {
      id: 12,
      message: "Username is missing.",
      code: 404
    },
    {
      id: 13,
      message: "The domain name of your email address is not allowed.",
      code: 401
    },
    {
      id: 14,
      message: "The firstname is missing.",
      code: 404
    },
    {
      id: 15,
      message: "The lastname is missing.",
      code: 404
    }
  ],
  server: [
    {
      id: 1,
      message: "An error on the server prevents validation of your session. Please try again later.",
      code: 500
    },
  ],
  service: [
    {
      id: 1,
      message: "The link is no longer valid. Thank you to reapply for a password.",
      code: 401
    },
    {
      id: 2,
      message: "No tokens were provided.",
      code: 404
    },
    {
      id: 3,
      message: "Task does not exist for this ID.",
      code: 404
    }
  ],
  database: [
    {
      id: 1,
      message: "",
      code: 500
    },
  ]
}

export const EN_SUCCESS_MESSAGE: ErrorMessage = {
  user: [
    {
      id: 1,
      message: "User has been created.",
      code: 201
    },
    {
      id: 2,
      message: "User has been updated.",
      code: 201
    },
    {
      id: 3,
      message: "User has been deleted.",
      code: 201
    },
    {
      id: 4,
      message: "User found.",
      code: 201
    },
    {
      id: 5,
      message: "Users found.",
      code: 201
    },
    {
      id: 6,
      message: "Connection successfull.",
      code: 201
    },
    {
      id: 7,
      message: "A new password request has been sent to the specified email address.",
      code: 201
    },
    {
      id: 8,
      message: "Your account has been created.",
      code: 201
    },
    {
      id: 9,
      message: "The password has been updated.",
      code: 201
    }
  ],
  server: [
    {
      id: 1,
      message: "The server is started and is listening on ",
      code: 201
    },
  ],
  service: [
    {
      id: 1,
      message: "Current list of tasks.",
      code: 201
    },
    {
      id: 2,
      message: "Task's informations.",
      code: 201
    },
    {
      id: 3,
      message: "Task has been deleted",
      code: 201
    }
  ],
  database: [
    {
      id: 1,
      message: "The connection with the Database is successfull.",
      code: 201
    },
  ]
}
