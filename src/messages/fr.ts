export const FR_ERROR_MESSAGE: ErrorMessage = {
  user: [
    {
      id: 1,
      message: "L'utilisateur n'existe pas.",
      code: 404
    },
    {
      id: 2,
      message: "Un utilisateur existe déjà avec ce pseudo ou cet email.",
      code: 409
    },
    {
      id: 3,
      message: "L'utilisateur n'a pas été mis à jour.",
      code: 409
    },
    {
      id: 4,
      message: "L'utilisateur n'a pas été supprimé.",
      code: 409
    },
    {
      id: 5,
      message: "Aucun utilisateur n'a été trouvé.",
      code: 404
    },
    {
      id: 6,
      message: "L'utilisateur ou le mot de passe n'est pas correctes.",
      code: 401
    },
    {
      id: 7,
      message: "Cette adresse email ne correspond à aucun utilisateur.",
      code: 404
    },
    {
      id: 8,
      message: "Vous n'avez pas l'authorisation nécessaire pour effectuer cette action.",
      code: 403
    },
    {
      id: 9,
      message: "Le mot de passe n'a pas été mis à jour.",
      code: 409
    },
    {
      id: 10,
      message: "L'adresse email est manquante.",
      code: 404
    },
    {
      id: 11,
      message: "Le mot de passe est manquant.",
      code: 404
    },
    {
      id: 12,
      message: "Le pseudo est manquant.",
      code: 404
    },
    {
      id: 13,
      message: "Le nom de domaine de votre adresse email n'est pas authorisé.",
      code: 401
    },
    {
      id: 14,
      message: "Le prénom est manquant.",
      code: 404
    },
    {
      id: 15,
      message: "Le nom de famille est manquant.",
      code: 404
    }
  ],
  server: [
    {
      id: 1,
      message: "Une erreur sur le serveur empêche la validation de votre session. Merci de réessayer plus tard.",
      code: 500
    }
  ],
  service: [
    {
      id: 1,
      message: "Le lien n'est plus valide. Merci de faire une nouvelle demande de mot de passe.",
      code: 401
    },
    {
      id: 2,
      message: "Aucun token n'a été fourni.",
      code: 404
    },
    {
      id: 3,
      message: "Aucune tâche n'existe pour cet ID.",
      code: 404
    }
  ],
  database: [
    {
      id: 1,
      message: "",
      code: 500
    }
  ]
};

export const FR_SUCCESS_MESSAGE: ErrorMessage = {
  user: [
    {
      id: 1,
      message: "L'utilisateur a été créé.",
      code: 201
    },
    {
      id: 2,
      message: "L'utilisateur a été mis à jour.",
      code: 201
    },
    {
      id: 3,
      message: "L'utilisateur a été supprimé.",
      code: 201
    },
    {
      id: 4,
      message: "Utilisateur trouvé.",
      code: 201
    },
    {
      id: 5,
      message: "Utilisateurs trouvés.",
      code: 201
    },
    {
      id: 6,
      message: "Connexion réussie.",
      code: 201
    },
    {
      id: 7,
      message: "Une demande de nouveau mot de passe a été envoyée à l'adresse email indiquée.",
      code: 201
    },
    {
      id: 8,
      message: "Votre compte a bien été créé.",
      code: 201
    },
    {
      id: 9,
      message: "Le mot de passe a été mis à jour.",
      code: 201
    }
  ],
  server: [
    {
      id: 1,
      message: "Le serveur est démarré et écoute sur le port ",
      code: 201
    }
  ],
  service: [
    {
      id: 1,
      message: "Liste des tâches en cours.",
      code: 201
    },
    {
      id: 2,
      message: "Informations de la tâche.",
      code: 201
    },
    {
      id: 3,
      message: "La tâche a été supprimée.",
      code: 201
    }
  ],
  database: [
    {
      id: 1,
      message: "Connection avec la base de données réussie.",
      code: 201
    }
  ]
};
