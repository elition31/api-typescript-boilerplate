// Packages
import * as express from "express";

// Controllers
import { UserController } from "./../controllers/user.controller";

// Routes
import Route from "../models/interfaces/route";

// Middlewares
import { checkJwt } from "../middlewares/checkJwts";
import { checkRole } from "../middlewares/checkRole";

class UserRoutes implements Route {
  public userController: UserController = new UserController();
  public path: string = "/:lang/users";
  public router = express.Router();

  constructor() {
    this.initRoutes()
  }

  /**
   * Initialize USER routes
   */
  public initRoutes(): void {
    this.router.get(`${this.path}/all`, [checkJwt, checkRole], this.userController.fetchUsers);
    this.router.get(`${this.path}/one/id/:id`, [checkJwt, checkRole], this.userController.fetchUserById);
    this.router.get(`${this.path}/one/email/:email`, [checkJwt, checkRole], this.userController.fetchUserByEmail);
    this.router.post(`${this.path}/add`, [checkJwt, checkRole], this.userController.addUser);
    this.router.put(`${this.path}/update/:id`, [checkJwt, checkRole], this.userController.updateUser);
    this.router.delete(`${this.path}/delete/:id`, [checkJwt, checkRole], this.userController.deleteUser);
  }
}

export default UserRoutes;