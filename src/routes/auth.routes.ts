// Packages
import * as express from "express";

// Controllers
import { AuthController } from "../controllers/auth.controller";

// Routes
import Route from "../models/interfaces/route";

// Middlewares
import { checkTokenPassword } from "../middlewares/checkJwts";

class AuthRoutes implements Route {
  public authController: AuthController = new AuthController();
  public path: string = "/:lang/auth";
  public router = express.Router();

  constructor() {
    this.initRoutes()
  }

  /**
   * Initialize USER routes
   */
  public initRoutes(): void {
    this.router.post(`${this.path}/login`, this.authController.login);
    this.router.post(`${this.path}/remember`, this.authController.rememberPassword);
    this.router.post(`${this.path}/register`, this.authController.register);
    this.router.post(`${this.path}/updatePassword`, [checkTokenPassword], this.authController.updateUserPassword);
  }
}
  
export default AuthRoutes;