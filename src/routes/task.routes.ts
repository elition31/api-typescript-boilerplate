// Packages
import * as express from "express";

// Controllers
import { TaskController } from "../controllers/task.controller";

// Routes
import Route from "../models/interfaces/route";

// Middlewares
import { checkJwt } from "../middlewares/checkJwts";
import { checkRole } from "../middlewares/checkRole";

class TaskRoutes implements Route {
  public taskController: TaskController = new TaskController();
  public path: string = "/:lang/task";
  public router = express.Router();

  constructor() {
    this.initRoutes()
  }

  /**
   * Initialize TASK routes
   */
  public initRoutes(): void {
    this.router.get(`${this.path}/all`, [checkJwt, checkRole], this.taskController.fetchTasks);
    this.router.get(`${this.path}/one/id/:id`, [checkJwt, checkRole], this.taskController.fetchTaskById);
    this.router.delete(`${this.path}/delete/:id`, [checkJwt, checkRole], this.taskController.deleteTask);
  }
}
  
export default TaskRoutes;