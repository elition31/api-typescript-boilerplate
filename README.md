# NodeJS API with Express and Typescript

# WIP

## Todo
1. Refresh Token system
2. Log all route
3. Configure Cors
4. Configure Brut force Protection
5. Documentation Open api
6. A lot of stuff !

## Test
1. Send New Password System [OK]

## Documentation
1. Auth Routes
  - login
  - register
  - remember
  - updatePassword
2. User Routes
  - all
  - one/id/:id
  - one/email/:email
  - add
  - update/:id
  - delete/:id
3. Task routes
  - all
  - one/id/:id
  - delete/:id
